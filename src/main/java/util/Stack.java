package util;

public class Stack<T> {

    /**
     * Max capacity of the stack
     */
    public static final int MAXSIZE = 10;

    /**
     * Items of the stack (over an array)
     */
    @SuppressWarnings("unchecked")
    private T[] items = (T[]) new Object[MAXSIZE];

    /**
     * Size of the stack (serves as cursor of stack limit in array)
     */
    private int size = 0;

    /**
     * Pushes an element onto the stack.
     * @param o is the object to store on top of the stack
     */
    public void push(T o) {
        if(size >= MAXSIZE) throw new IllegalStateException("Stack exceeded capacity!");
        items[size] = o;
        size++;
    }

    /**
     * Removes and retrieves the top element of the stack
     * @return the top element of the stack
     */
    public T pop() {
        if (size == 0) throw new IllegalStateException("empty stack");
        size--;
        return items[size];
    }

    /**
     * Retrieves the top element of the stack, without removing it
     * @return the top element of the stack
     */
    public T top() {
        if (size == 0) throw new IllegalStateException("empty stack");
        return items[size-1];
    }

    /**
     * Checks whether the stack is empty
     * @return true iff stack is empty.
     */
    public boolean isEmpty() {
        return (size==0);
    } 

    /**
     * Returns the size of the stack.
     * @return the number of elements in the stack.
     */
    public int size() {
        return size;
    }

    /**
     * Produces a string representation of the stack
     * @return a string representation of the stack
     */
    public String toString() {
        String result = "[";
        for (int i = 0; i < this.size; i++) {
            result += (items[i] == null ? "null": items[i].toString());
            if (i < this.size-1) result += ", ";
        }
        result += "]";
        return result;
    }

}
