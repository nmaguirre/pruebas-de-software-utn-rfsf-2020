package realBugs;

public class ZuneBug {

	/**
	 * Computes current year, given the number of days passed since Jan 1 1980.
	 * @param days is the number of days passed since Jan 1 1980.
	 * @return the corresponding (current) year.
	 */
	public static int zunebug(int days) {
		if (days < 0) throw new IllegalArgumentException("positive days, please");
		int year = 1980;
		while (days > 365) {
			if (isLeapYear(year)) {
				if (days > 366) {
					days -= 366;
					year += 1; 
				}
				else {
					days -= 366;
				}
			}
			else {
				days -= 365;
				year += 1;
			}
		}
		return year;
	}

	/**
	 * Checks whether a given year is a leap year.
	 * @param year is the year to query about.
	 * @return true iff year is a leap year.
	 */
	public static boolean isLeapYear(int year) {
		boolean result = false;
		if (year % 400 == 0) {
			result = true;
		}
		else {
			if (year % 100 == 0) {
				result = false;
			}
			else {
				result = (year % 4 == 0);
			}
		}	
		return result;
	}
}
