package mail;

import java.util.HashSet;
import java.util.Set;

public class FiltroDeCorreos {
    
    /**
     * DOC: depended-on component 
     * DUC: depended-upon component
     * El sistema de correos sobre el que se aplica el filtro
     */
    private SistemaDeCorreo sistema;
    
    /**
     * Lista negra de destinatarios 
     */
    private Set<String> listaNegra = new HashSet<String>();
    
    public FiltroDeCorreos(SistemaDeCorreo sistema) {
        if (sistema == null) throw new IllegalArgumentException("sistema invalido");
        this.sistema = sistema;
    }
    
    /**
     * Retorna el sistema al cual aplica el filtro
     * @return
     */
    public SistemaDeCorreo getSistema() {
        return this.sistema;
    }
    
    /**
     * bloquea un destinatario
     * @param destinatario
     */
    public void bloquear(String destinatario) {
        listaNegra.add(destinatario);
    }
    
    /**
     * desbloquea un destinatario
     * @param destinatario
     */
    public void desbloquear(String destinatario) {
        listaNegra.remove(destinatario);
    }
    
    /**
     * pregunta si un destinatario esta bloqueado
     * @param destinatario
     * @return
     */
    public boolean estaBloqueado(String destinatario) {
        return listaNegra.contains(destinatario);
    }
    
    /**
     * Procesa un email del sistema teniendo en cuenta la lista negra
     */
    public void procesar() {
        if (sistema.tienePorProcesar()) {
            Mensaje mensaje = sistema.eliminarPrimeroPorProcesar();
            if (!this.estaBloqueado(mensaje.obtenerDestinatario())) {
                sistema.enviar(mensaje);
            }
        }
        else {
            // no hago nada, no hay nada por procesar
        }
    }
    
}
