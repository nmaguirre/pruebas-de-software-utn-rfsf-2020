package mail;

/**
 * Interfaz que representa un msg de correo electronico
 * @author aguirre
 *
 */
public interface Mensaje {

    /**
     * Retorna el destinatario del mensaje
     * @return
     */
    String obtenerDestinatario();

}
