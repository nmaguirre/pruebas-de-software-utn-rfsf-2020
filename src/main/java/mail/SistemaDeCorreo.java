package mail;

/**
 * Representa el sistema de correo, con varias funcionalidades
 * (abstraccion de cola de mensajes no procesados, por enviar..)
 * Tambien me permite enviar mensajes...
 * @author aguirre
 *
 */
public interface SistemaDeCorreo {

    /**
     * Existen mensajes (salientes) por procesar?
     * @return
     */
    boolean tienePorProcesar();

    /**
     * retornar (y eliminar) el primer mensaje por procesar
     * de la cola de msgs por procesar
     * @return
     */
    Mensaje eliminarPrimeroPorProcesar();

    /**
     * Envia un mensaje
     * @param actual
     */
    void enviar(Mensaje actual);
    
}
