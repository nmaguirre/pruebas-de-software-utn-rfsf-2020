package simpleExamples;

/**
 * Simple static routines over basic or simple structured types.
 *
 */
public class SampleStaticRoutines {


	/**
	 * Finds the last position of value in array. Returns -1 if item
	 * does not belong to the array.
	 * @param array the array to search in
	 * @param value is the value whose last position will be searched for
	 * @return the maximum index in input holding value, -1 if value is not in the array
	 */
	public static int findLastPosition(int[] array, int value) {
		if (array == null) throw new IllegalArgumentException("Cannot search in null array");
		int maxIndex = -1;
		int currentIndex = 0;
		while (currentIndex != array.length) {
			if (array[currentIndex] == value) maxIndex = currentIndex;
			currentIndex++;
		}
		return maxIndex;
	}

	/**
	 * Counts the number of even numbers in an array.
	 * @param array the array to count the number of even on.
	 * @return the number of even numbers in the array.
	 */
	public static int countEven(Integer[] array) {
		int numberOfEven = 0;
		for (int i = 0; i < array.length; i++) {
			if (array[i] % 2 != 0) numberOfEven++;
		}
		return numberOfEven;
	}

	/**
	 * Returns the max object between three objects.
	 * @param x is one of the elements to compare.
	 * @param y is one of the elements to compare.
	 * @param z is one of the elements to compare.
	 * @return the max object between x, y and z.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static Comparable max(Comparable x, Comparable y, Comparable z) {
		Comparable result = null;
		if (x.compareTo(y) >= 0 && x.compareTo(z) >= 0) result = x;
		if (y.compareTo(x) >= 0 && y.compareTo(z) >= 0) result = y;
		else result = z;
		return result;
	}

	/**
	 * Sorts an array in increasing order.
	 * @param the array to be sorted.
	 */	
	public static void sortArray(int[] array) {
		for (int l = array.length-1; l > 1; l--) {
			for (int i = 0; i < l; i++) {
				if (array[i] > array[i+1]) {
					int e = array[i];
					array[i] = array[i+1];
					array[i+1] = e;
				}
			}
		}
	}

	/**
	 * Computes the maximum between two integers.
	 * @param x is one of the elements to compare.
	 * @param y is one of the elements to compare.
	 * @return the maximum between x and y.
	 */
	public static int max(int x, int y) {
		if (x >= y) return x;
		return y;
	}
	
	public static int factorial(int i) {
		int fact = 1;
		for (int j = 1; j <= i; j++) {
			fact *= j;
		}
		return fact;
	}
}