package quickcheck;

import com.pholser.junit.quickcheck.generator.GenerationStatus;
import com.pholser.junit.quickcheck.generator.Generator;
import com.pholser.junit.quickcheck.random.SourceOfRandomness;

import util.Stack;

public class StackGenerator extends Generator<util.Stack<Integer>> {

    public StackGenerator(Class<Stack<Integer>> type) {
        super(type);
    }

    @Override
    public Stack<Integer> generate(SourceOfRandomness random, GenerationStatus status) {
        // nula o no nula?
        boolean genNull = random.nextBoolean();
        if (genNull) {
            return null;
        }
        else {
            // tamaño de la pila?
            int numberOfElements = Math.abs(random.nextInt())% Stack.MAXSIZE;

            Stack<Integer> stack = new Stack<Integer>();
            for (int i = 0; i < numberOfElements; ++i) {
                // push random integer
                stack.push(random.nextInt()%Stack.MAXSIZE); 
            }
            return stack;
        }
    }
}
