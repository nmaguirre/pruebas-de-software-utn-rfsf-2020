package quickcheck;

import util.Stack;

import com.pholser.junit.quickcheck.From;
import com.pholser.junit.quickcheck.Property;
import com.pholser.junit.quickcheck.runner.JUnitQuickcheck;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;
import static org.junit.Assume.assumeTrue;

@RunWith(JUnitQuickcheck.class)
public class StackProperties {

    /**
     * Adding an element to a stack that is not full, increases its size by one
     */
    @Property 
	public void addingElementIncreasesSize(@From(StackGenerator.class) Stack<Integer> stack, int elem) {
		
    	assumeTrue(stack != null);
		assumeTrue("stack is not full", stack.size() < Stack.MAXSIZE);
		
		System.out.println(stack.toString());
				
		int oldSize = stack.size();
		
		stack.push(elem);
		
		assertEquals("size increased by one", oldSize + 1, stack.size());
	}
	

    /**
     * Removing an element decreases size by one, if stack is not empty.
     */
    @Property 
	public void extractingElementDecreasesSize(@From(StackGenerator.class) Stack<Integer> stack) {
				    	
    	assumeTrue(stack != null);
		assumeTrue("la pila no esta vacia", !stack.isEmpty());

	    System.out.println(stack.toString());

		
		int oldSize = stack.size();
		
		stack.pop();
		
		assertEquals("size decreased by one", oldSize - 1, stack.size());
	}

}
