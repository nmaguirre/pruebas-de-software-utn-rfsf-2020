package realBugs;

import static org.junit.Assert.*;

import org.junit.Test;

public class ZuneBugTest {

	@Test
	public void testIsLeapYear0() {
		int anho = 2020;
		
		boolean resultado = ZuneBug.isLeapYear(anho);
		
		assertTrue(resultado);
	}

	@Test
	public void testIsLeapYear1() {
		int anho = 400;
		
		boolean resultado = ZuneBug.isLeapYear(anho);
		
		assertTrue(resultado);
	}

	@Test
	public void testIsLeapYear2() {
		int anho = 100;
		
		boolean resultado = ZuneBug.isLeapYear(anho);
		
		assertFalse(resultado);
	}
	
	@Test
	public void testIsLeapYear3() {
		int anho = 2019;
		
		boolean resultado = ZuneBug.isLeapYear(anho);
		
		assertFalse(resultado);
	}
	
	@Test
	public void testZuneBug0() {
		int cantDias = 1;
		
		int anho = ZuneBug.zunebug(cantDias);
		
		assertEquals(1980, anho);
	}

	@Test
	public void testZuneBug00() {
		int cantDias = 0;
		
		int anho = ZuneBug.zunebug(cantDias);
		
		assertEquals(1980, anho);
	}

	@Test
	public void testZuneBugGrosso() {
		int cantDias = 366;
		
		int anho = ZuneBug.zunebug(cantDias);
		
		assertEquals(1980, anho);
	}

	@Test
	public void testZuneBugPasoUnAnho() {
		int cantDias = 367;
		
		int anho = ZuneBug.zunebug(cantDias);
		
		assertEquals(1981, anho);
	}

	@Test
	public void testZuneBugPasaronCasiDos() {
		int cantDias = 732;
		
		int anho = ZuneBug.zunebug(cantDias);
		
		assertEquals(1982, anho);
	}

	@Test
	public void testZuneBugPorquePITQuiere() {
		int cantDias = 365;
		
		int anho = ZuneBug.zunebug(cantDias);
		
		assertEquals(1980, anho);
	}

	
	@Test(expected=IllegalArgumentException.class)
	public void testZuneBugNegativo() {
		int cantDias = -1;
		
		int anho = ZuneBug.zunebug(cantDias);
	}
	
	
}
