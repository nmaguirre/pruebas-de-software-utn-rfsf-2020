package simpleExamples;

import java.util.Random;

import org.junit.Test;
import org.junit.experimental.theories.DataPoint;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.experimental.theories.suppliers.TestedOn;
import org.junit.runner.RunWith;

import simpleExamples.SampleStaticRoutines;
import static org.junit.Assert.*;
import static org.junit.Assume.*;


@RunWith(Theories.class)
public class TeoriaCountEven {
		
	
	@Theory 
	public void countEvenSmallerThanSize(Integer[] array) {
		
		if (array != null) {
			int result = SampleStaticRoutines.countEven(array);
		
			assertTrue(result <= array.length);
		}
	}

	@Theory 
	public void countEvenGreaterThanZero(Integer[] array) {
		
		// assumeThat(array!=null)
		int result = SampleStaticRoutines.countEven(array);
		
		assertTrue(result >= 0);
		
	}

	
}
