package simpleExamples;

import static org.junit.Assert.*;

import org.junit.Test;

public class SampleStaticRoutinesFactorialTest {

	@Test
	public void factorialOfOneTest() {
		int i = 1;
		
		int fact = SampleStaticRoutines.factorial(i);
		
		assertEquals(1, fact);
	}

	@Test
	public void factorialOfTwotest() {
		int i = 2;
		
		int fact = SampleStaticRoutines.factorial(i);
		
		assertEquals(2, fact);
	}

	@Test
	public void factorialOfThreetest() {
		int i = 3;
		
		int fact = SampleStaticRoutines.factorial(i);
		
		assertEquals(6, fact);
	}

	
}
