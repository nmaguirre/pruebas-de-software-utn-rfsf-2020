package simpleExamples;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized.Parameters;
import org.junit.runners.Parameterized;

@RunWith(Parameterized.class)
public class SampleSRParameterizedTest {

	private Integer[] array;
	private Integer expectedResult;
	
	public SampleSRParameterizedTest(Object [] array, Object expectedResult) {
		this.array = (Integer[]) array;
		this.expectedResult = (Integer) expectedResult;
	}
	
	//arrange
	@Parameterized.Parameters
	public static Collection primeNumbers() {
	      return Arrays.asList(new Object[][] {
	         { new Integer[] {1}, 0 },
	         { new Integer[] {1, 2, 3}, 1 },
	         { new Integer[] {2}, 1 },
	         { new Integer[] {4, 2}, 0 },
	         { new Integer[] {3, 3}, 0 }
	      });
	   }
	
	@Test
	public void countEvenTest() {
	    
	    // act
		int actualResult = SampleStaticRoutines.countEven((Integer[]) array);
		
		// assert
		org.junit.Assert.assertEquals((Integer) expectedResult, (Integer) actualResult);
	}

}