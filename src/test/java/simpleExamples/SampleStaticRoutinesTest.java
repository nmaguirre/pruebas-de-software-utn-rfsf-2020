package simpleExamples;

import static org.junit.Assert.*;

import org.junit.Test;

public class SampleStaticRoutinesTest {

	@Test
	public void testFindLastPositionElemNotInArray() {
		// arrange
		int[] array = {0, 0, 0};
		int value = 1;
		
		// act
		int result = SampleStaticRoutines.findLastPosition(array, value);
		
		// assert
		assertEquals(-1, result);
	}

	
	@Test
	public void testFindLastPositionElemInArray() {
		// arrange
		int[] array = {0, 1, 2};
		int value = 0;
		
		// act
		int result = SampleStaticRoutines.findLastPosition(array, value);
		
		// assert
		assertEquals(0, result);
		
	}
	
	//@Test
	public void testCountEvenEmptyArray() {
		// arrange
		int[] array = { };
		
		// act
		//int res = SampleStaticRoutines.countEven(array);
		
		// assert
		// assertEquals(0, res);
		
	}
	
}
