package mail;

import static org.easymock.EasyMock.*;
import static org.junit.Assert.*;

import static org.junit.Assert.*;

import org.junit.Test;

public class FiltroDeCorreosTest {

    @Test
    public void testConstructor() {
        // creo sistema "dummy"
        SistemaDeCorreo sistema = createMock(SistemaDeCorreo.class);

        FiltroDeCorreos filtro = new FiltroDeCorreos(sistema);

        assertEquals(sistema, filtro.getSistema());     
    }

    @Test(expected=IllegalArgumentException.class)
    public void testConstructorInvalidSystem() {
        SistemaDeCorreo sistema = null; // doble de prueba (vacio)

        FiltroDeCorreos filtro = new FiltroDeCorreos(sistema);

    }

    @Test
    public void testBloquear() {
        // creo sistema "dummy"
        SistemaDeCorreo sistema = createMock(SistemaDeCorreo.class);

        FiltroDeCorreos filtro = new FiltroDeCorreos(sistema);
        String dest = "Naza";
        
        filtro.bloquear(dest);
        
        assertTrue(filtro.estaBloqueado(dest));     
    }

    @Test
    public void testDesbloquear() {
        // creo sistema "dummy"
        SistemaDeCorreo sistema = createMock(SistemaDeCorreo.class);
        FiltroDeCorreos filtro = new FiltroDeCorreos(sistema);
        String dest = "Naza";
        filtro.bloquear(dest);
        
        filtro.desbloquear(dest);
        
        assertFalse(filtro.estaBloqueado(dest));     
    }

    @Test
    public void testProcesarConBloqueo() {
        // mensaje ficticio con destinatario Naza
        Mensaje mensajeDestNaza = createMock(Mensaje.class);
        expect(mensajeDestNaza.obtenerDestinatario()).andReturn("Naza");
        replay(mensajeDestNaza);
       
        // sistema ficticio
        // tiene por procesar? SI
        // msg por procesar? mensajeDestNaza (mirar arriba)
        SistemaDeCorreo sistema = createMock(SistemaDeCorreo.class);
        expect(sistema.tienePorProcesar()).andReturn(true);
        expect(sistema.eliminarPrimeroPorProcesar()).andReturn(mensajeDestNaza);
        replay(sistema);
        
        FiltroDeCorreos filtro = new FiltroDeCorreos(sistema);
        String dest = "Naza";
        filtro.bloquear(dest);
                
        filtro.procesar();
        
        verify(sistema);
        
    }

    @Test
    public void testProcesarSinBloqueo() {
        // mensaje ficticio (dest Naza)
        Mensaje mensajeDestNaza = createMock(Mensaje.class);
        expect(mensajeDestNaza.obtenerDestinatario()).andReturn("Naza");
       
        replay(mensajeDestNaza);
        
        // sistema ficticio
        SistemaDeCorreo sistema = createNiceMock(SistemaDeCorreo.class);
        expect(sistema.tienePorProcesar()).andReturn(true);
        expect(sistema.eliminarPrimeroPorProcesar()).andReturn(mensajeDestNaza);
        sistema.enviar(mensajeDestNaza);
        expectLastCall().times(1);
        replay(sistema);
        
        FiltroDeCorreos filtro = new FiltroDeCorreos(sistema);
        String dest = "Cecilia";
        filtro.bloquear(dest);
        
        filtro.procesar();
        
        // assert (chequea las expectativas de los mocks)
        verify(sistema);
        
    }

    @Test
    public void testProcesarNoHayPorProcesar() {
        SistemaDeCorreo sistema = createMock(SistemaDeCorreo.class);
        expect(sistema.tienePorProcesar()).andReturn(false);
        replay(sistema);
        
        FiltroDeCorreos filtro = new FiltroDeCorreos(sistema);
        String dest = "Cecilia";
        filtro.bloquear(dest);
        
        filtro.procesar();
        
        verify(sistema);
    }


}
