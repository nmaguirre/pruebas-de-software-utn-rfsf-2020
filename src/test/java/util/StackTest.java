package util;

import java.util.ArrayList;
import org.junit.experimental.theories.DataPoint;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;
import static org.junit.Assume.*;

@RunWith(Theories.class)
public class StackTest {

	@DataPoint
	public static Stack<Integer> nullStack = null;
	    
    @DataPoint
    public static Stack<Integer> emptyStack = new Stack<Integer>();
    
    @DataPoints
    public static ArrayList<Stack<Integer>> stacks() {
        ArrayList<Stack<Integer>> stacks = new ArrayList<Stack<Integer>>();
        int numStacks = 5;
        for (int i = 0; i < numStacks; i++) {
            Stack<Integer> stack = new Stack<Integer>();
            for (int j = 0; j <= i; j++) {
                stack.push(j);
            }
            stacks.add(stack);
        }
        return stacks;
    } 
    
    @DataPoint
    public static Stack<Integer> fullStack() {
    	Stack<Integer> stack = new Stack<Integer>();
    	while (stack.size() < Stack.MAXSIZE) {
    		stack.push(0);
    	}
    	return stack;
    }
    
    @DataPoints
    public static int[] variousElements = { 0, 1, 2, 3}; 
    
    /**
     * Adding an element to a stack that is not full, increases its size by one
     */
    @Theory 
	public void addingElementIncreasesSize(Stack<Integer> stack, int elem) {
		
    	assumeTrue(stack != null);
		assumeTrue("stack is not full", stack.size() < Stack.MAXSIZE);
		
		int oldSize = stack.size();
		
		stack.push(elem);
		
		assertEquals("size increased by one", oldSize + 1, stack.size());
	}
	
	// TODO: Pensar en una propiedad que involucre los metodos de Stack
	// y capturar la misma como un test parametrizado adicional.
    
    @Theory 
	public void extractingElementDecreasesSize(Stack<Integer> stack) {
				    	
    	assumeTrue(stack != null);
		assumeTrue("la pila no esta vacia", !stack.isEmpty());
				
		int oldSize = stack.size();
		
		stack.pop();
		
		assertEquals("size decreased by one", oldSize - 1, stack.size());
	}

}
