# language: es

Característica: El juego reacciona a los movimientos básicos, y a la lógica del juego, correctamente. 
				Esta característica corresponde a la reacción correcta al comando down (mover hacia abajo).
				Tests construidos usando black-box (each choice) a partir de las propiedades
				1 mover me generaría el 2048 esté en el tablero (boolean) 
				2 que tenga celdas libres (boolean)
				3 que tenga celdas adyacentes horizontales con el mismo valor (boolean)  
				4 que tenga celdas adyacentes horizontales con distinto valor (boolean) 
				5 que tenga celdas adyacentes verticales con el mismo valor (boolean)
				6 que tenga celdas adyacentes verticales con distinto valor (boolean)
				7 mover para abajo me genera una nueva celda aleatoria (NO, CUATRO, DOS)
				8 el juego termino (no puedo mover y tablero lleno) --> tengo 2048, o no puedo mover y tablero lleno (no libres, no adyacentes vert ni horizontales)

#- ACLARACION: adyacentes es que estén pegadas o tengan celdas libres que las separen
#- ACLARACION: celda vacía no cuenta como valor (ni para iguales, ni para distintos)
#- RESTRICCIONES
#- P1 = True => P8 = False
#- P1 = True => P7 = NO
#- P1 = True => P5 = True
#- P8 = True => (P1 = False && P7 = NO)
#- P2 = False => (P3 = True || P4 = True)
#- P2 = False => (P5 = True || P6 = True)



	Antecedentes:
		Dado que la aplicación ha sido iniciada
		Y el juego ha sido iniciado   
               
				@ApiTest
        Escenario: Cubro la tupla  T T T T T T NO F   		
            Dado el tablero está en el estado
                        | 2| 2|  |      |
                        | 4|  | 2| 1024 |
                        |  |  |  |      |
                        |  |  |  | 1024 |
            Cuando ejecuto el comando down
            Entonces debería obtener exactamente el tablero
                        |  |  |  |      |
                        |  |  |  |      |
                        | 2|  |  |      |
                        | 4| 2| 2| 2048 |
            Y la aplicación debería informarme que gané.


				@ApiTest
        Escenario: Cubro la tupla T T T T T F NO F    		
            Dado el tablero está en el estado
                        | 2| 2|  |      |
                        |  |  | 2| 1024 |
                        |  |  |  |      |
                        |  |  |  | 1024 |
            Cuando ejecuto el comando down
            Entonces debería obtener exactamente el tablero
                        |  |  |  |      |
                        |  |  |  |      |
                        |  |  |  |      |
                        | 2| 2| 2| 2048 |
            Y la aplicación debería informarme que gané.
 
	             
				@ApiTest
        Escenario: Cubro la tupla F T T F F F DOS F    		
            Dado el tablero está en el estado
                        | 2| 2|  |      |
                        |  |  | 2|      |
                        |  |  |  |      |
                        |  |  |  | 1024 |
            Cuando ejecuto el comando down
            Y las coordenadas aleatorias de la nueva celda a cargar son (x,y)
            Y el valor aleatorio para la celda es 2
            Entonces debería obtener el tablero
                        |  |  |  |      |
                        |  |  |  |      |
                        |  |  |  |      |
                        | 2| 2| 2| 1024 |
            Y (x,y) deben ser diferentes a (3,3)
            Y (x,y) deben ser diferentes a (3,2)
            Y (x,y) deben ser diferentes a (3,1)
            Y (x,y) deben ser diferentes a (3,0)
            Y el tablero en la posición (x,y) debería estar cargado con 2.												

            
				@ApiTest
        Escenario: Cubro la tupla F T T F F F CUATRO F    		
            Dado el tablero está en el estado
                        | 2| 2|  |      |
                        |  |  | 2|      |
                        |  |  |  |      |
                        |  |  |  | 1024 |
            Cuando ejecuto el comando down
            Y las coordenadas aleatorias de la nueva celda a cargar son (x,y)
            Y el valor aleatorio para la celda es 4
            Entonces debería obtener el tablero
                        |  |  |  |      |
                        |  |  |  |      |
                        |  |  |  |      |
                        | 2| 2| 2| 1024 |
            Y (x,y) deben ser diferentes a (3,3)
            Y (x,y) deben ser diferentes a (3,2)
            Y (x,y) deben ser diferentes a (3,1)
            Y (x,y) deben ser diferentes a (3,0)
            Y el tablero en la posición (x,y) debería estar cargado con 4.												


				@ApiTest
        Escenario: Cubro la tupla F F T F T F CUATRO F   		
            Dado el tablero está en el estado
                        | 2| 2| 2| 2|
                        | 2| 2| 2| 2|
                        | 2| 2| 2| 2|
                        | 2| 2| 2| 2|
            Cuando ejecuto el comando down
            Y las coordenadas aleatorias de la nueva celda a cargar son (x,y)
            Y el valor aleatorio para la celda es 4    
            Entonces debería obtener el tablero
                        |  |  |  |  |
                        |  |  |  |  |
                        | 4| 4| 4| 4|
                        | 4| 4| 4| 4|
            Y (x,y) deben ser diferentes a (3,3)
            Y (x,y) deben ser diferentes a (3,2)
            Y (x,y) deben ser diferentes a (3,1)
            Y (x,y) deben ser diferentes a (3,0)
            Y (x,y) deben ser diferentes a (2,3)
            Y (x,y) deben ser diferentes a (2,2)
            Y (x,y) deben ser diferentes a (2,1)
            Y (x,y) deben ser diferentes a (2,0) 
            Y el tablero en la posición (x,y) debería estar cargado con 4.			

				@ApiTest
        Escenario: Cubro la tupla F F T F T F DOS F   		
            Dado el tablero está en el estado
                        | 2| 2| 2| 2|
                        | 2| 2| 2| 2|
                        | 2| 2| 2| 2|
                        | 2| 2| 2| 2|
            Cuando ejecuto el comando down
            Y las coordenadas aleatorias de la nueva celda a cargar son (x,y)
            Y el valor aleatorio para la celda es 2    
            Entonces debería obtener el tablero
                        |  |  |  |  |
                        |  |  |  |  |
                        | 4| 4| 4| 4|
                        | 4| 4| 4| 4|
            Y (x,y) deben ser diferentes a (3,3)
            Y (x,y) deben ser diferentes a (3,2)
            Y (x,y) deben ser diferentes a (3,1)
            Y (x,y) deben ser diferentes a (3,0)
            Y (x,y) deben ser diferentes a (2,3)
            Y (x,y) deben ser diferentes a (2,2)
            Y (x,y) deben ser diferentes a (2,1)
            Y (x,y) deben ser diferentes a (2,0) 
            Y el tablero en la posición (x,y) debería estar cargado con 2.			

                        
				@ApiTest
        Escenario: Cubro la tupla F T F F F T NO F   		
            Dado el tablero está en el estado
                        |  |  |  |  |
                        |  |  |  |  |
                        |  |  |  | 4|
                        |  |  |  | 2|
            Cuando ejecuto el comando down
            Entonces debería obtener exactamente el tablero
                        |  |  |  |  |
                        |  |  |  |  |
                        |  |  |  | 4|
                        |  |  |  | 2|

		
				@ApiTest
        Escenario: Cubro la tupla F T F F F T NO F   		
            Dado el tablero está en el estado
                        |  |  |  |     |
                        |  |  |  |     |
                        |  |  |  |     |
                        |  |  |  | 2048|
            Entonces el sistema debería informarme que no puedo mover para abajo

